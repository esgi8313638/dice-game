const socket = io();

const logbook = document.getElementById('logbook')
const diceBtn = document.getElementById('roll');
const holdBtn = document.getElementById('hold')
const actions = document.getElementById('actions');

let currentPlayer;
let activePlayer;

socket.on('currentPlayer', (player) => {
    currentPlayer = player.playerId === socket.id;
    activePlayer = player.activePlayer
    console.log('activePlayer: ', activePlayer)
});

diceBtn.addEventListener('click', () => {
    console.log('Dice Roll!');
    socket.emit('doTheRoll');
});

socket.on('rollScore', (diceRollScore) => {
    document.getElementById('dice-score-' + activePlayer).innerText = diceRollScore.diceRoll;

    const log = document.createElement('li');
    if (diceRollScore.playerId !== socket.id) {
        log.textContent = `Opponent roll dice made: ${diceRollScore.diceRoll}`;
        log.style.backgroundColor = '#f1c40f';
        log.style.color = '#ecf0f1';
        log.style.padding = '10px';
    } else {
        log.textContent = `Roll dice made: ${diceRollScore.diceRoll}`;
        log.style.backgroundColor = '#3498db';
        log.style.color = '#ecf0f1';
        log.style.padding = '10px';
    }
    logbook.appendChild(log);
    console.log(activePlayer);
    document.getElementById('current-score-' + activePlayer).innerText = diceRollScore.playerCurrentScore;
});

holdBtn.addEventListener('click', () => {
    console.log(activePlayer);
    socket.emit('holdTheScore', { socketId: socket.id, activePlayer });
    socket.emit('switchPlayer');
});

socket.on('holdScore', (score) => {
    const log = document.createElement('li');

    if (score.playerId !== socket.id) {
        log.textContent = `Opponent roll dice made: ${score.playerScore}`;
        log.style.backgroundColor = '#f1c40f';
        log.style.color = '#ecf0f1';
        log.style.padding = '10px';
    } else {
        log.textContent = `Roll dice made: ${score.playerScore}`;
        log.style.backgroundColor = '#3498db';
        log.style.color = '#ecf0f1';
        log.style.padding = '10px';
    }

    document.getElementById('final-score-' + activePlayer).innerText = score.playerScore;
    socket.emit('switchPlayer');
});



socket.on('switchPlayer', (currentScore) => {
    document.getElementById('current-score-' + activePlayer).innerText = currentScore;
    console.log('activePlayer: ', activePlayer, 'currentPlayer: ', currentPlayer);
    // if (currentPlayer) {
    //     actions.classList.remove('hide');
    // } else {
    //     actions.classList.add('hide');
    // }
});

socket.on('user-disconnected', () => {
    document.getElementById('dice-score-' + activePlayer).innerText = 0;
    document.getElementById('current-score-' + activePlayer).innerText = 0;
    document.getElementById('final-score-' + activePlayer).innerText = 0;

    alert('A user is disconnected');
});