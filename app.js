const express = require('express');
const { log } = require('node:console');
const { createServer } = require('node:http');
const { join } = require('node:path');

const { Server } = require('socket.io');

const app = express();
const server = createServer(app);

const io = new Server(server);

app.use(express.static(join(__dirname, 'public')));

app.get("/", (req, res) => {
    res.sendFile(join(__dirname, 'public/index.html'))
});

let activePlayer = 0;

const player = [
    {
        currentScore: 0,
        score: 0
    },
    {
        currentScore: 0,
        score: 0
    }
];

io.on('connection', (socket) => {
    console.log('user connected: ', socket.id);
    console.log(activePlayer);
    socket.emit('currentPlayer', { playerId: socket.id, activePlayer });

    socket.on('doTheRoll', () => {
        const diceRoll = Math.floor(Math.random() * 6) + 1

        if (diceRoll !== 1) {
            player[activePlayer].currentScore += diceRoll;
            const playerCurrentScore = player[activePlayer].currentScore;
            io.emit('rollScore', { diceRoll, playerCurrentScore, playerId: socket.id });
        } else {
            const playerCurrentScore = 0;
            io.emit('rollScore', { diceRoll, playerCurrentScore, playerId: socket.id });

            const playerScore = player[activePlayer].score;
            io.emit('holdScore', { playerScore, playerId: socket.id })

            player[activePlayer].currentScore = 0;
            activePlayer = 1 - activePlayer;
            io.emit('currentPlayer', { playerId: socket.id, activePlayer });
        }
    });

    socket.on('holdTheScore', (playerHoldScore) => {
        player[activePlayer].score += player[activePlayer].currentScore;
        const playerScore = player[activePlayer].score;
        let socketId = playerHoldScore.socketId;
        io.emit('holdScore', { playerScore, playerId: socket.id, socketId });
    });

    socket.on('switchPlayer', () => {
        player[activePlayer].currentScore = 0;
        activePlayer = 1 - activePlayer;
        io.emit('currentPlayer', { playerId: socket.id, activePlayer });

        const playerCurrentScore = player[activePlayer].currentScore;
        io.emit('switchPlayer', playerCurrentScore);
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
        player[0].currentScore = 0;
        player[0].score = 0;

        player[1].currentScore = 0;
        player[1].score = 0;

        io.emit('user-disconnected')
    });
});

server.listen(3000, () => {
    console.log('server running at http://localhost:3000');
});